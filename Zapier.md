## Zoom new events
This automation takes a new event created in Zoom and populates the Google Calendar for zen@the-exhale.com

Zoom account: Zen
https://zapier.com/shared/549c9d2b7c322d6339d74f7daec76f2079c617e7

Zoom account: Ruth
https://zapier.com/shared/11448b6999654e0a322772cc98ab31162b28ab7b

Zoom account: Gwendolyn
https://zapier.com/shared/0cbe770f27c23e299f916c0f25f47e6db00182a6


## Notification of new Video in Faculty Form
Dm4g3GW2j3wnBBgTu5p3

https://zapier.com/shared/1df8edd29e6b6445997c32d25c2820070755cbf5
